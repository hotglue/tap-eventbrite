"""Stream type classes for tap-eventbrite."""

from typing import Optional

from singer_sdk import typing as th

from tap_eventbrite.client import eventbriteStream

class OrganizationsStream(eventbriteStream):
    """Define Organizations stream."""
    name = "organizations"
    path = "users/me/organizations/"
    primary_keys = ["id"]
    records_jsonpath = "$.organizations[*]"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("vertical", th.StringType),
        th.Property("image_id", th.StringType),
        th.Property("parent_id", th.StringType),
        th.Property("locale", th.StringType)
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"organization_id": record["id"]}

class EventsStream(eventbriteStream):
    """Define Events stream."""
    name = "events"
    path = "organizations/{organization_id}/events/"
    primary_keys = ["id"]
    records_jsonpath = "$.events[*]"
    parent_stream_type = OrganizationsStream

    schema = th.PropertiesList(
        th.Property("name",
            th.ObjectType(
                th.Property("text", th.StringType),
                th.Property("html", th.StringType)
            )
        ),
        th.Property("description",
            th.ObjectType(
                th.Property("text", th.StringType),
                th.Property("html", th.StringType)
            )
        ),
        th.Property("url", th.StringType),
        th.Property("start",
            th.ObjectType(
                th.Property("timezone", th.StringType),
                th.Property("local", th.DateTimeType),
                th.Property("utc", th.DateTimeType)
            )
        ),
        th.Property("end",
            th.ObjectType(
                th.Property("timezone", th.StringType),
                th.Property("local", th.DateTimeType),
                th.Property("utc", th.DateTimeType)
            )
        ),
        th.Property("organization_id", th.StringType),
        th.Property("created", th.DateTimeType),
        th.Property("changed", th.DateTimeType),
        th.Property("published", th.DateTimeType),
        th.Property("capacity", th.IntegerType),
        th.Property("capacity_is_custom", th.BooleanType),
        th.Property("status", th.StringType),
        th.Property("currency", th.StringType),
        th.Property("listed", th.BooleanType),
        th.Property("shareable", th.BooleanType),
        th.Property("invite_only", th.BooleanType),
        th.Property("password", th.StringType),
        th.Property("online_event", th.BooleanType),
        th.Property("show_remaining", th.BooleanType),
        th.Property("tx_time_limit", th.IntegerType),
        th.Property("hide_start_date", th.BooleanType),
        th.Property("hide_end_date", th.BooleanType),
        th.Property("locale", th.StringType),
        th.Property("is_locked", th.BooleanType),
        th.Property("privacy_setting", th.StringType),
        th.Property("is_series", th.BooleanType),
        th.Property("is_series_parent", th.BooleanType),
        th.Property("inventory_type", th.StringType),
        th.Property("is_reserved_seating", th.BooleanType),
        th.Property("show_pick_a_seat", th.BooleanType),
        th.Property("show_seatmap_thumbnail", th.BooleanType),
        th.Property("show_colors_in_seatmap_thumbnail", th.BooleanType),
        th.Property("source", th.StringType),
        th.Property("is_free", th.BooleanType),
        th.Property("version", th.StringType),
        th.Property("summary", th.StringType),
        th.Property("facebook_event_id", th.StringType),
        th.Property("logo_id", th.StringType),
        th.Property("organizer_id", th.StringType),
        th.Property("venue_id", th.StringType),
        th.Property("category_id", th.StringType),
        th.Property("subcategory_id", th.StringType),
        th.Property("format_id", th.StringType),
        th.Property("id", th.StringType),
        th.Property("resource_uri", th.StringType),
        th.Property("is_externally_ticketed", th.BooleanType),
        th.Property("logo", th.StringType)
    ).to_dict()

class AttendeesStream(eventbriteStream):
    """Define Attendees stream."""
    name = "attendees"
    path = "organizations/{organization_id}/attendees/"
    primary_keys = ["id", "changed"]
    records_jsonpath = "$.attendees[*]"
    replication_key = "changed"
    parent_stream_type = OrganizationsStream
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("created", th.DateTimeType),
        th.Property("changed", th.DateTimeType),
        th.Property("ticket_class_id", th.StringType),
        th.Property("ticket_class_name", th.StringType),
        th.Property("profile", th.CustomType({"type": ["object", "string"]})),
        th.Property("questions", th.CustomType({"type": ["array", "string"]})),
        th.Property("answers", th.CustomType({"type": ["array", "string"]})),
        th.Property("barcodes", th.CustomType({"type": ["array", "string"]})),
        th.Property("team", th.CustomType({"type": ["object", "string"]})),
        th.Property("affiliate", th.StringType),
        th.Property("checked_in", th.BooleanType),
        th.Property("cancelled", th.BooleanType),
        th.Property("refunded", th.BooleanType),
        th.Property("costs", th.CustomType({"type": ["object", "string"]})),
        th.Property("status", th.StringType),
        th.Property("event_id", th.StringType),
        th.Property("event", th.CustomType({"type": ["object", "string"]})),
        th.Property("order_id", th.StringType),
        th.Property("order", th.CustomType({"type": ["object", "string"]})),
        th.Property("guestlist_id", th.StringType),
        th.Property("invited_by", th.StringType),
        th.Property("assigned_unit", th.CustomType({"type": ["object", "string"]})),
        th.Property("delivery_method", th.StringType),
        th.Property("variant_id", th.StringType),
        th.Property("contact_list_preferences", th.CustomType({"type": ["object", "string"]})),
        th.Property("resource_uri", th.StringType),
    ).to_dict()