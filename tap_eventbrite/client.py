"""REST client handling, including eventbriteStream base class."""

import requests
from typing import Any, Dict, Optional

from singer_sdk.streams import RESTStream
from singer_sdk.authenticators import APIKeyAuthenticator


class eventbriteStream(RESTStream):
    """eventbrite stream class."""

    url_base = "https://www.eventbriteapi.com/v3/"

    @property
    def authenticator(self) -> APIKeyAuthenticator:
        """Return a new authenticator object."""
        api_key = self.config.get("api_key")
        return APIKeyAuthenticator.create_for_stream(
            self,
            key="Authorization",
            value= f"Bearer {api_key}",
            location="header"
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        paginated = response.json().get("pagination")
        if not paginated:
            return None
        if paginated.get("has_more_items"):
            return paginated.get("continuation")
        return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["continuation"] = next_page_token
        if self.replication_key:
            start_date = self.get_starting_timestamp(context)
            if start_date:
                date = start_date.strftime("%Y-%m-%dT%H:%M:%SZ")
                params["changed_since"] = date
        return params
