"""eventbrite tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th
from tap_eventbrite.streams import (
    OrganizationsStream,
    EventsStream,
    AttendeesStream
)


STREAM_TYPES = [
    OrganizationsStream,
    EventsStream,
    AttendeesStream
]


class Tapeventbrite(Tap):
    """eventbrite tap class."""
    name = "tap-eventbrite"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync"
        )
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__=="__main__":
    Tapeventbrite.cli()